# Kubernetes on Scaleway

This project was made to follow [Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way/) but with a twist !

I want to automate every step, infrastructure provisioning is done with Terraform and configuration with Ansible

This is part of my personal training for the Kubernetes CKA certifiation and is still a Work In Progress ⚠️
