```bash
ansible all -m ping -i inventory.yml -u ansible --private-key ../keys/ansible

ansible-galaxy install -r requirements.yml

ansible-playbook playbook.yml -i inventory.yml --private-key ../keys/ansible
```