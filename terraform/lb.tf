resource "scaleway_lb_ip" "ip" {
}

resource "scaleway_lb" "lb" {
  ip_id  = scaleway_lb_ip.ip.id
  type   = "LB-S"
}

resource "scaleway_lb_frontend" "kubeapi_frontend" {
  lb_id        = scaleway_lb.lb.id
  backend_id   = scaleway_lb_backend.kubeapi_backend.id
  name         = "Kubernetes API"
  inbound_port = "80"
}

resource "scaleway_lb_backend" "kubeapi_backend" {
  lb_id            = scaleway_lb.lb.id
  name             = "Kubernetes API"
  forward_protocol = "http"
  forward_port     = "80"
  server_ips       = scaleway_instance_server.kube_controllers.*.ipv6_address
}