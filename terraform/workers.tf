locals {
  workers_number = 1
}

resource "scaleway_instance_server" "kube_workers" {
  count       = local.workers_number
  name        = "worker-${count.index}"
  type        = "DEV1-S"
  image       = "debian_buster"
  enable_ipv6 = true

  user_data = {
    cloud-init = file("${path.module}/cloud-init.yml")
  }

}

resource "scaleway_instance_private_nic" "pnic02" {
  count              = local.workers_number
  server_id          = scaleway_instance_server.kube_workers[count.index].id
  private_network_id = scaleway_vpc_private_network.vpc_network.id
}