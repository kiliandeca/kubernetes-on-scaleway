locals {
  controllers = scaleway_instance_server.kube_controllers
  workers = scaleway_instance_server.kube_workers
}

resource "local_file" "inventory" {
  content = templatefile("inventory.tmpl",
    {
      lb = scaleway_lb_ip.ip
      controllers = scaleway_instance_server.kube_controllers,
      workers = scaleway_instance_server.kube_workers,
    }
  )
  filename = "../ansible/inventory.yml"
}
