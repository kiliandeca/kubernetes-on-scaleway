locals {
  controllers_number = 3
}

resource "scaleway_instance_server" "kube_controllers" {
  count       = local.controllers_number
  name        = "controller-${count.index}"
  type        = "DEV1-S"
  image       = "debian_buster"
  enable_ipv6 = true

  user_data = {
    cloud-init = file("${path.module}/cloud-init.yml")
  }

}

resource "scaleway_instance_private_nic" "pnic01" {
  count              = local.controllers_number
  server_id          = scaleway_instance_server.kube_controllers[count.index].id
  private_network_id = scaleway_vpc_private_network.vpc_network.id
}
